<!doctype html>
<?php
    if(isset($_POST["submit"]))
    {
        require_once 'vektor.php';
        $a=new point(0,0);
        $b=new point(0,0);
        $c=new point(0,0);
        $d=new point(0,0);
    }
?>
<html lang="cs">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Body v prostoru</title> 
    <link rel="stylesheet" href="style.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="jsxgraph.css" />
    <script type="text/javascript" src="jsxgraphcore.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    </head>
<body class="container">
<header>
    <h1>Body v prostoru</h1>
</header>    
    <div class="row">
    <form class="form col-md-12" method="post" action="index.php">
            <div class="col-xs-10 col-md-6">
                <input type="number" class="form-control" name="A1" placeholder="A1" value=
                <?php if(isset($_POST["submit"]))
                {
                    $a->prvni($_POST['A1']);
                }
                ?>>
            </div>
            <div class="col-xs-10 col-md-6">
                <input type="number" class="form-control" name="A2" placeholder="A2" value=
                <?php if(isset($_POST["submit"]))
                    {
                    $a->druhy($_POST['A2']);
                    }
                ?>>
            </div>
            <div class="col-xs-10 col-md-6">
                <input type="number" class="form-control" name="B1" placeholder="B1" value=
                <?php if(isset($_POST["submit"]))
                    {
                    $b->prvni($_POST['B1']);
                    }
                ?>>
            </div>
            <div class="col-xs-10 col-md-6">
                <input type="number" class="form-control" name="B2" placeholder="B2" value=
                <?php if(isset($_POST["submit"]))
                    {
                    $b->druhy($_POST['B2']);
                    }
                ?>>
            </div>
            <div class="col-xs-10 col-md-6">
                <input type="number" class="form-control" name="PX" placeholder="Posun na X" value=<?php if(isset($_POST["submit"]))
                    {
                        $c->zmena_x($_POST['PX'],$a);
                        $d->zmena_x($_POST['PX'],$b);
                    }
                    ?>
                >
            </div>
            <div class="col-xs-10 col-md-6">
                <input type="number" class="form-control" name="PY" placeholder="Posun na Y" value=<?php if(isset($_POST["submit"]))
                    {
                        $c->zmena_y($_POST['PY'],$a);
                        $d->zmena_y($_POST['PY'],$b);
                    }
                    ?>
                >
            </div>
            <div class="col-xs-10 col-md-6 col-md-offset-10">
                    <input id="submit" name="submit" type="submit" value="Odeslat" class="btn btn-primary">
            </div>
    </form>
    </div>
    
    <?php if(isset($_POST["submit"]))
    {
    ?>
    <div class="row">
    <div class="alert alert-success col-xs-8 col-md-12 container">
        <strong>Vzdálenost zadaných bodů <?php echo $a->vypis();?> a <?php echo $b->vypis();?> = <?php echo $a->vzdalenost($b);?></strong>
    </div>
    </div>
    <div class="row">
    <div class="vypis">
        <h2>Změna</h2>
        <h3>Před změnou</h3>
    
        <p>A: <?php echo $a->vypis();?></p>
        <p>B: <?php echo $b->vypis();?></p>        
    
        <h3>Po posunu</h3>
    
        <p>C: <?php echo $c->vypis();?></p>
        <p>D: <?php echo $d->vypis();?></p>
    </div>

    <div class="graf">
        <div id="box" class="jxgbox" style="width:500px; height:500px;"></div>
            <script type="text/javascript">
                var brd = JXG.JSXGraph.initBoard('box',{axis:true,boundingbox:[-10,10,10,-10]});
 
                var A = brd.create('point', [<?php $a->prvni_vypis(); ?>,<?php $a->druhy_vypis(); ?>]);
                var B = brd.create('point', [<?php $b->prvni_vypis(); ?>,<?php $b->druhy_vypis(); ?>]);
                var C = brd.create('point', [<?php $c->prvni_vypis(); ?>,<?php $c->druhy_vypis(); ?>]);
                var D = brd.create('point', [<?php $d->prvni_vypis(); ?>,<?php $d->druhy_vypis(); ?>]);
 
                brd.on('move', function(){
                        A.moveTo([A.X(), A.Y()]);
                        B.moveTo([B.X(), B.Y()]);
                        C.moveTo([C.X(), C.Y()]);
                        D.moveTo([D.X(), D.Y()]);
                });
            </script>
    </div>
    </div>
    <?php
    }
    ?>
</body>
</html>